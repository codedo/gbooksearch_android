package ru.test.gbookssearch.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import ru.test.gbookssearch.R;
import ru.test.gbookssearch.fragments.FavoriteFragment;
import ru.test.gbookssearch.fragments.SearchFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.toolbar));
        BottomNavigationView menu = findViewById(R.id.navigation);
        menu.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case (R.id.menu_search):
                    return openFragment(new SearchFragment());
                case (R.id.menu_favorite):
                    return openFragment(new FavoriteFragment());
            }
            return false;
        });
        openFragment(new SearchFragment());
    }

    private boolean openFragment(Fragment fragment) {
        if (getCurrentFragment() == null || !getCurrentFragment().getClass().getName().equals(fragment.getClass().getName())) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            if (getCurrentFragment() != null) {
                ft.remove(getCurrentFragment());
            }
            ft.add(R.id.frame_layout, fragment);
            ft.commit();
            return true;
        }
        return false;
    }

    private Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.frame_layout);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        invalidateOptionsMenu();
    }
}
