package ru.test.gbookssearch.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import ru.test.gbookssearch.entities.Book;

/**
 * Created by Vit on 12.03.2018.
 * базовый интерфейс вью для экранов "поиск" и "избранное"
 */

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MvpBaseView extends MvpView{

    void showEmptyView(boolean isVisible);

    void showData(boolean isVisible);

    void showErrorView(boolean isVisible);

    void showLoadingView(boolean isVisible);

    void setupData(List<Book> repoInfoList);
}
