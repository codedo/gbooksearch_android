package ru.test.gbookssearch.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import ru.test.gbookssearch.entities.Book;

/**
 * Created by Vit on 09.03.2018.
 * view для экрана поиска
 */
public interface MvpSearchView extends MvpBaseView {

    @StateStrategyType(SkipStrategy.class)
    void setupSearchListener(String query);
}
