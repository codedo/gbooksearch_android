package ru.test.gbookssearch.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Vit on 10.03.2018.
 * базовый адаптер
 */

public abstract class BaseAdapter<O, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private List<O> itemList;

    @Override
    public int getItemCount() {
        return itemList != null ? itemList.size() : 0;
    }

    protected O getItem(int position) {
        if (itemList != null && itemList.size() > position) {
            return itemList.get(position);
        }
        return null;
    }

    public void setItems(List<O> list) {
        itemList = list;
    }

    protected View inflateLayout(ViewGroup parent, int resId) {
        return LayoutInflater.from(parent.getContext())
                .inflate(resId, parent, false);
    }
}