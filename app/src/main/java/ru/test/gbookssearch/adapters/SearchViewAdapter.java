package ru.test.gbookssearch.adapters;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.test.gbookssearch.R;
import ru.test.gbookssearch.entities.Book;
import ru.test.gbookssearch.view_holders.BookVH;

/**
 * Created by Vit on 10.03.2018.
 * адаптер для списка репозиториев
 */

public class SearchViewAdapter extends BaseAdapter<Book, BookVH> {

    private BookVH.Actions actionListener;

    @NonNull
    @Override
    public BookVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BookVH(inflate(R.layout.item_book_info, parent));

    }

    private View inflate(int layoutResId, ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);
    }

    @Override
    public void onBindViewHolder(@NonNull BookVH holder, int position) {
        holder.onBind(getItem(position), actionListener);
    }

    public void setActionListener(BookVH.Actions actionListener) {
        this.actionListener = actionListener;
    }
}
