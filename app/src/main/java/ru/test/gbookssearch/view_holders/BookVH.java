package ru.test.gbookssearch.view_holders;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ru.test.gbookssearch.R;
import ru.test.gbookssearch.entities.Book;

/**
 * Created by Vit on 10.03.2018.
 * Vh для информации о репозитории
 */

public class BookVH extends RecyclerView.ViewHolder {

    private TextView title,
            authors;
    private View previewButton;
    private ImageView imageView,
            favorite;

    public interface Actions {
        void onPrevButtonClick(String link);

        void onFavoriteClick(Book book);
    }

    public BookVH(View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.title_field_text);
        authors = itemView.findViewById(R.id.author_field_text);
        previewButton = itemView.findViewById(R.id.preview);
        imageView = itemView.findViewById(R.id.image);
        favorite = itemView.findViewById(R.id.favorite);
    }

    public void onBind(Book book, Actions listener) {
        title.setText(book.getTitle());
        authors.setText(TextUtils.isEmpty(book.getAuthors()) ? "-" : book.getAuthors());
        Picasso.with(imageView.getContext())
                .load(book.getImageLink())
                .placeholder(R.drawable.image_placeholder)
                .into(imageView);
        previewButton.setOnClickListener((v) -> {
            if (listener != null) {
                listener.onPrevButtonClick(book.getPreviewLink());
            }
        });
        favorite.setImageResource(book.isFavorite() ? R.drawable.ic_favorite : R.drawable.ic_favorite_border);
        favorite.setOnClickListener((view) -> {
            book.setFavorite(!book.isFavorite());
            if (listener != null) {
                listener.onFavoriteClick(book);
            }
        });
    }
}
