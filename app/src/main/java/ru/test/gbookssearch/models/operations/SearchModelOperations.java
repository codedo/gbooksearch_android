package ru.test.gbookssearch.models.operations;

import java.util.List;

import ru.test.gbookssearch.R;
import ru.test.gbookssearch.entities.Book;
import ru.test.gbookssearch.helpers.ResourcesHelper;
import ru.test.gbookssearch.models.LoadListener;

/**
 * Created by Vit on 09.03.2018.
 * интерфейс модели поисковых запросов
 */

public interface SearchModelOperations {

    int PER_PAGE = 10;

    void search(String query, LoadListener<List<Book>> listener);

    void stopCall();
}
