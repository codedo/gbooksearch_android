package ru.test.gbookssearch.models;

/**
 * Created by Vit on 09.03.2018.
 * универсальный слушатель загрзки
 */

public interface LoadListener<O> {

    void onLoad(O data);

    void onError(String error);
}
