package ru.test.gbookssearch.models.operations;

import java.util.List;

import ru.test.gbookssearch.entities.Book;
import ru.test.gbookssearch.models.LoadListener;

/**
 * Created by Vit on 12.03.2018.
 * интерфейс модели для работы с "избранное"
 */

public interface FavoriteModelOperations {

    void setFavorite(Book book, LoadListener<Book> listener);

    void getFavBooks(LoadListener<List<Book>> listener);
}
