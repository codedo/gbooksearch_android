package ru.test.gbookssearch.models;

import java.util.List;

import ru.test.gbookssearch.entities.Book;
import ru.test.gbookssearch.helpers.RealmHelper;
import ru.test.gbookssearch.models.operations.FavoriteModelOperations;

/**
 * Created by Vit on 12.03.2018.
 * модель для работы с "избранное"
 */

public class FavoriteModel implements FavoriteModelOperations {

    @Override
    public void setFavorite(Book book, LoadListener<Book> listener) {
        RealmHelper.setBookFavorite(book);
        if (listener != null) {
            listener.onLoad(book);
        }
    }

    @Override
    public void getFavBooks(LoadListener<List<Book>> listener) {
        if (listener != null) {
            listener.onLoad(RealmHelper.getFavoriteBooks());
        }
    }
}
