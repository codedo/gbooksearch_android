package ru.test.gbookssearch.models;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.test.gbookssearch.entities.Book;
import ru.test.gbookssearch.entities.SearchRequestWrapper;
import ru.test.gbookssearch.entities.Volume;
import ru.test.gbookssearch.models.operations.SearchModelOperations;
import ru.test.gbookssearch.retrofit.RetrofitManager;

/**
 * Created by Vit on 09.03.2018.
 * модель для поисковых запросов
 */

public class SearchModel implements SearchModelOperations {

    private Call<SearchRequestWrapper> call;

    @Override
    public void search(String query,
                       final LoadListener<List<Book>> listener) {
        stopCall();
        call = RetrofitManager.getInstance()
                .getBooksByQuery(0, PER_PAGE, query);
        call.enqueue(new Callback<SearchRequestWrapper>() {
            @Override
            public void onResponse(@NonNull Call<SearchRequestWrapper> call,
                                   @NonNull Response<SearchRequestWrapper> response) {
                if (listener != null) {
                    SearchRequestWrapper wrapper = response.body();
                    if (response.isSuccessful() && wrapper != null) {
                        List<Book> list = new ArrayList<>();
                        if (wrapper.getItems() != null) {
                            for (Volume vol : wrapper.getItems()) {
                                Book book = new Book(vol);
                                //book.setFavorite(RealmHelper.containsBookById(book.getId()));
                                list.add(book);
                            }
                        }
                        listener.onLoad(list);
                    } else {
                        listener.onError(null);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchRequestWrapper> call,
                                  @NonNull Throwable t) {
                if (listener != null && !call.isCanceled()) {
                    listener.onError(t.getLocalizedMessage());
                }
            }
        });
    }

    @Override
    public void stopCall() {
        if (call != null) {
            call.cancel();
        }
    }
}
