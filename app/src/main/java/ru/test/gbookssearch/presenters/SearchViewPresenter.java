package ru.test.gbookssearch.presenters;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import ru.test.gbookssearch.entities.Book;
import ru.test.gbookssearch.models.FavoriteModel;
import ru.test.gbookssearch.models.LoadListener;
import ru.test.gbookssearch.models.SearchModel;
import ru.test.gbookssearch.models.operations.SearchModelOperations;
import ru.test.gbookssearch.views.MvpSearchView;

/**
 * Created by Vit on 09.03.2018.
 * презентер для экрана поиска
 */
@InjectViewState
public class SearchViewPresenter extends BasePresenter<MvpSearchView> {

    private SearchModelOperations model;

    private String query;

    public SearchViewPresenter() {
        this.model = new SearchModel();
        this.favModel = new FavoriteModel();
    }

    public void onSearch() {
        model.stopCall();
        if (query == null || query.length() == 0) {
            showEmptyView();
        } else {
            showLoadingView();
            model.search(query, new LoadListener<List<Book>>() {
                @Override
                public void onLoad(List<Book> data) {
                    books = data;
                    onLoaded();
                }

                @Override
                public void onError(String error) {
                    showErrorView();
                }
            });
        }
    }

    public void onSearchTextChange(String query) {
        this.query = query;
    }

    public void onPostMenu() {
        getViewState().setupSearchListener(query);
    }

    @Override
    public void onErrorViewRepeatClick() {
        onSearch();
    }
}