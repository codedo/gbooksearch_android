package ru.test.gbookssearch.presenters;

import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import ru.test.gbookssearch.entities.Book;
import ru.test.gbookssearch.models.LoadListener;
import ru.test.gbookssearch.models.operations.FavoriteModelOperations;
import ru.test.gbookssearch.views.MvpBaseView;

/**
 * Created by Vit on 12.03.2018.
 * базовый презентер для экранов "поиск" и "избранное"
 */

public abstract class BasePresenter<V extends MvpBaseView> extends MvpPresenter<V> {

    protected List<Book> books;

    protected FavoriteModelOperations favModel;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        showEmptyView();
    }

    protected void showEmptyView() {
        getViewState().showEmptyView(true);
        getViewState().showData(false);
        getViewState().showErrorView(false);
        getViewState().showLoadingView(false);
    }

    protected void showData() {
        getViewState().setupData(books);
        getViewState().showEmptyView(false);
        getViewState().showData(true);
        getViewState().showErrorView(false);
        getViewState().showLoadingView(false);
    }

    protected void showErrorView() {
        getViewState().showEmptyView(false);
        getViewState().showData(false);
        getViewState().showErrorView(true);
        getViewState().showLoadingView(false);
    }

    protected void showLoadingView() {
        getViewState().showEmptyView(false);
        getViewState().showData(false);
        getViewState().showErrorView(false);
        getViewState().showLoadingView(true);
    }

    protected void onLoaded() {
        if (books != null && books.size() > 0) {
            showData();
        } else {
            showEmptyView();
        }
    }

    public void onBookFavClick(Book book) {
        if (book != null) {
            favModel.setFavorite(book, new LoadListener<Book>() {
                @Override
                public void onLoad(Book data) {
                    updateData(book);
                    onLoaded();
                }

                @Override
                public void onError(String error) {

                }
            });
        }
    }

    private void updateData(Book book) {
        if (books != null) {
            for (int i = 0; i < books.size(); i++) {
                if (books.get(i).getId().equals(book.getId())) {
                    books.remove(i);
                    books.add(i, book);
                    return;
                }
            }
        }
    }

    public abstract void onErrorViewRepeatClick();
}
