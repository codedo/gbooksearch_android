package ru.test.gbookssearch.presenters;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import ru.test.gbookssearch.entities.Book;
import ru.test.gbookssearch.models.FavoriteModel;
import ru.test.gbookssearch.models.LoadListener;
import ru.test.gbookssearch.views.MvpBaseView;

/**
 * Created by Vit on 12.03.2018.
 * презентер для экрана "избранное"
 */
@InjectViewState
public class FavoritePresenter extends BasePresenter<MvpBaseView> {

    public FavoritePresenter() {
        favModel = new FavoriteModel();
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadFavBooks();
    }

    private void loadFavBooks() {
        favModel.getFavBooks(new LoadListener<List<Book>>() {
            @Override
            public void onLoad(List<Book> data) {
                books = data;
                onLoaded();
            }

            @Override
            public void onError(String error) {
                showErrorView();
            }
        });
    }

    @Override
    public void onErrorViewRepeatClick() {
        loadFavBooks();
    }
}
