package ru.test.gbookssearch.entities;

import android.widget.CompoundButton;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Vit on 12.03.2018.
 * книга
 */

public class Book extends RealmObject{

    @PrimaryKey
    private String id;
    private String title;
    private String authors;
    private String previewLink;
    private String imageLink;
    @Index
    private boolean favorite;

    public Book() {
    }

    public Book(Volume volume) {
        this.id = volume.getId();
        this.title = volume.getTitle();
        this.authors = volume.getAuthors();
        this.previewLink = volume.getPreviewLink();
        this.imageLink = volume.getImageLink();
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthors() {
        return authors;
    }

    public String getPreviewLink() {
        return previewLink;
    }

    public String getImageLink() {
        return imageLink;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
