package ru.test.gbookssearch.entities;

import java.util.List;

/**
 * Created by Vit on 10.03.2018.
 * обертка для ответа от сервера в запросе поиска
 */

public class SearchRequestWrapper {

    private String kind;
    private long totalItems;
    private List<Volume> items;

    public long getTotalItems() {
        return totalItems;
    }

    public List<Volume> getItems() {
        return items;
    }
}
