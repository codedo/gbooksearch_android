package ru.test.gbookssearch.entities;

import android.text.TextUtils;

import java.util.List;

/**
 * Created by Vit on 12.03.2018.
 * обертка позиции в списке в запросе
 */

public class Volume {

    private String id;
    private VolumeInfo volumeInfo;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return volumeInfo != null ? volumeInfo.getTitle() : null;
    }

    public String getAuthors() {
        String authors = null;
        if (volumeInfo != null && volumeInfo.getAuthors() != null && !volumeInfo.getAuthors().isEmpty()) {
            authors = "";
            for (String author : volumeInfo.getAuthors()) {
                if (!TextUtils.isEmpty(authors)) {
                    authors += ", ";
                }
                authors += author;
            }
        }
        return authors;
    }

    public String getDescription() {
        return volumeInfo != null ? volumeInfo.getDescription() : null;
    }

    public String getImageLink() {
        return volumeInfo != null ? volumeInfo.getImageLink() : null;
    }

    public String getPreviewLink() {
        return volumeInfo != null ? volumeInfo.getPreviewLink() : null;
    }
}
