package ru.test.gbookssearch.entities;

import java.util.List;

/**
 * Created by Vit on 12.03.2018.
 * обертка информации о книге
 */

class VolumeInfo {
    private String title;
    private List<String> authors;
    private String description;
    private ImageLink imageLinks;
    private String previewLink;

    String getTitle() {
        return title;
    }

    List<String> getAuthors() {
        return authors;
    }

    String getDescription() {
        return description;
    }

    String getPreviewLink() {
        return previewLink;
    }

    String getImageLink() {
        return imageLinks != null ? imageLinks.getSmallThumbnail() : null;
    }
}