package ru.test.gbookssearch.entities;

/**
 * Created by Vit on 12.03.2018.
 * обертка для ссылок на изображения
 */

class ImageLink {
    private String smallThumbnail;
    private String thumbnail;

    String getSmallThumbnail() {
        return smallThumbnail;
    }
}