package ru.test.gbookssearch;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Vit on 11.03.2018.
 * custom application
 */

public class CustomApp extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("gbooksearch.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(config);
        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }
}
