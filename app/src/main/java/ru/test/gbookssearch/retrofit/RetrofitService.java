package ru.test.gbookssearch.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
import ru.test.gbookssearch.entities.SearchRequestWrapper;

/**
 * Created by Vit on 10.03.2018.
 * сетевые запросы
 */

public interface RetrofitService {

    @GET("books/v1/volumes")
    Call<SearchRequestWrapper> getBooksByQuery(@Query("startIndex") int page,
                                               @Query("maxResults") int perPage,
                                               @Query("q") String query);
}
