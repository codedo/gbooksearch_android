package ru.test.gbookssearch.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import ru.test.gbookssearch.CustomApp;

/**
 * Created by Vit on 11.03.2018.
 * класс-помощник для работы с SharedPref
 */

public class SharedPrefHelper {

    private static final String SHARED_PREF_NAME = "GitHubSearchShared";
    private static final String AUTH_TOKEN = "auth_token";
    private static final String DO_NOT_SHOW_AUTH_SCREEN = "doNotShowAuthScreen";


    private static SharedPreferences getDefault() {
        return CustomApp.getContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public static String getAuthToken() {
        return getDefault().getString(AUTH_TOKEN, null);
    }

    public static void removeAuthToken() {
        getDefault().edit().remove(AUTH_TOKEN).apply();
    }

    public static void setAuthToken(String token) {
        getDefault().edit().putString(AUTH_TOKEN, token).apply();
    }

    public static boolean isAuth() {
        return !TextUtils.isEmpty(getDefault().getString(AUTH_TOKEN, null));
    }

    public static boolean isDoNotShowAuthScreen() {
        return getDefault().getBoolean(DO_NOT_SHOW_AUTH_SCREEN, false);
    }

    public static void setDoNotShowAuthScreen(boolean show) {
        getDefault().edit().putBoolean(DO_NOT_SHOW_AUTH_SCREEN, show).apply();
    }
}
