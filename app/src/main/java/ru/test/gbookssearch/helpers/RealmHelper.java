package ru.test.gbookssearch.helpers;

import android.annotation.SuppressLint;
import android.support.annotation.UiThread;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import ru.test.gbookssearch.entities.Book;

/**
 * Created by Vit on 12.03.2018.
 * класс-помощник для работы с REalm
 */

public class RealmHelper {

    @SuppressLint("StaticFieldLeak")
    private static Realm realm;

    @UiThread
    private static Realm getRealm() {
        if (realm == null || realm.isClosed()) {
            realm = Realm.getDefaultInstance();
        }
        return realm;
    }

    public static List<Book> getFavoriteBooks() {
        Realm realm = getRealm();
        realm.beginTransaction();
        RealmResults<Book> books = realm.where(Book.class).equalTo("favorite", false).findAll();
        for (Book book : books) {
            book.deleteFromRealm();
        }
        books = realm.where(Book.class).equalTo("favorite", true).findAll();
        List<Book> list = realm.copyFromRealm(books);
        realm.commitTransaction();
        return list;
    }

    public static void setBookFavorite(Book book) {
        Realm realm = getRealm();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(book);
        realm.commitTransaction();
    }
}