package ru.test.gbookssearch.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ru.test.gbookssearch.R;

/**
 * Created by Vit on 11.03.2018.
 * класс для работы с датами
 */

public class DateHelper {

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String DAY_FORMAT_FOR_SCREEN = "dd.MM.yyyy";

    /**
     * конвертирует строку со временем в формате UTC в UTC Date
     *
     * @param StrDate строка со временем в UTC
     * @return date в local timeZone
     */
    private static Date stringUTCDateToDate(String StrDate) {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        try {
            dateToReturn = dateFormat.parse(StrDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateToReturn;
    }

    /**
     * отображает дату в виде "вчера", "сегодня" или дата
     *
     * @return "вчера"/"сегодня"/дата
     */
    public static String formatToYesterdayOrToday(String date) {
        Date dateTime = stringUTCDateToDate(date);
        if (dateTime != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateTime);
            Calendar today = Calendar.getInstance();
            Calendar yesterday = Calendar.getInstance();
            yesterday.add(Calendar.DATE, -1);
            SimpleDateFormat dateFormat = new SimpleDateFormat(DAY_FORMAT_FOR_SCREEN, Locale.US);
            if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
                    calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
                return ResourcesHelper.getString(R.string.today);
            } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) &&
                    calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
                return ResourcesHelper.getString(R.string.yesterday);
            } else {
                return dateFormat.format(dateTime);
            }
        } else return date;
    }
}
