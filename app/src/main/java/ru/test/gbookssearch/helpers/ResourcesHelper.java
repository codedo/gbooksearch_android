package ru.test.gbookssearch.helpers;

import ru.test.gbookssearch.CustomApp;

/**
 * Created by Vit on 11.03.2018.
 * класс-помощник для работы с ресурсами
 */

public class ResourcesHelper {

    public static String getString(int resId) {
        return CustomApp.getContext().getString(resId);
    }
}
