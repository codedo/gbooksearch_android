package ru.test.gbookssearch.fragments;

import com.arellomobile.mvp.presenter.InjectPresenter;

import ru.test.gbookssearch.presenters.BasePresenter;
import ru.test.gbookssearch.presenters.FavoritePresenter;
import ru.test.gbookssearch.views.MvpBaseView;

/**
 * Created by Vit on 12.03.2018.
 * фрагмент "избранное"
 */

public class FavoriteFragment extends BaseFragment implements MvpBaseView {

    @Override
    protected BasePresenter getBasePresenter() {
        return presenter;
    }

    @InjectPresenter
    public FavoritePresenter presenter;
}
