package ru.test.gbookssearch.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;

import java.util.List;

import ru.test.gbookssearch.R;
import ru.test.gbookssearch.adapters.SearchViewAdapter;
import ru.test.gbookssearch.entities.Book;
import ru.test.gbookssearch.presenters.BasePresenter;
import ru.test.gbookssearch.view_holders.BookVH;
import ru.test.gbookssearch.views.MvpBaseView;

/**
 * Created by Vit on 12.03.2018.
 * базовый фрагмент для экранов "поиск" и "избранное"
 */

public abstract class BaseFragment extends MvpAppCompatFragment implements MvpBaseView {

    protected View loadingView,
            errorView,
            emptyView;
    protected RecyclerView recyclerView;
    protected SearchViewAdapter adapter;

    protected abstract BasePresenter getBasePresenter();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadingView = view.findViewById(R.id.loading);

        recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new SearchViewAdapter();
        adapter.setActionListener(new BookVH.Actions() {
            @Override
            public void onPrevButtonClick(String link) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
            }

            @Override
            public void onFavoriteClick(Book book) {
                getBasePresenter().onBookFavClick(book);
            }
        });
        recyclerView.setAdapter(adapter);

        errorView = view.findViewById(R.id.error_view);
        view.findViewById(R.id.error_view_repeat_button).setOnClickListener(view1 -> getBasePresenter().onErrorViewRepeatClick());

        emptyView = view.findViewById(R.id.empty);
    }

    @Override
    public void showEmptyView(boolean isVisible) {
        emptyView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showData(boolean isVisible) {
        recyclerView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showErrorView(boolean isVisible) {
        errorView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showLoadingView(boolean isVisible) {
        loadingView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setupData(List<Book> repoInfoList) {
        adapter.setItems(repoInfoList);
        adapter.notifyDataSetChanged();
    }
}