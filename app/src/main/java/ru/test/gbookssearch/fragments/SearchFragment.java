package ru.test.gbookssearch.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.Timer;
import java.util.TimerTask;

import ru.test.gbookssearch.R;
import ru.test.gbookssearch.presenters.BasePresenter;
import ru.test.gbookssearch.presenters.SearchViewPresenter;
import ru.test.gbookssearch.views.MvpSearchView;

/**
 * Created by Vit on 08.03.2018.
 * Фрагмент с поиском.
 */

public class SearchFragment extends BaseFragment implements MvpSearchView {

    private static final int MIN_QUERY_LENGTH = 3;
    private static final long INPUT_SEARCH_DELAY_MS = 500;

    private SearchView searchView;
    private MenuItem searchItem;
    private Timer inputTimer;

    @InjectPresenter
    public SearchViewPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
        initSearchItem(menu);
        searchView.post(() -> presenter.onPostMenu());
    }

    private void initSearchItem(Menu menu) {
        searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchItem.getActionView();
        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
                                                 @Override
                                                 public boolean onMenuItemActionExpand(MenuItem item) {
                                                     searchView.onActionViewExpanded();
                                                     return true;
                                                 }

                                                 @Override
                                                 public boolean onMenuItemActionCollapse(MenuItem item) {
                                                     searchView.setQuery(null, true);
                                                     onSearchTextChange(null);
                                                     onSearch();
                                                     return true;
                                                 }
                                             }
        );
    }

    private void onSearch() {
        presenter.onSearch();
    }

    private void onSearchTextChange(String query) {
        presenter.onSearchTextChange(query);
    }

    @Override
    public void onDestroyView() {
        if (inputTimer != null) {
            inputTimer.cancel();
            inputTimer = null;
        }
        super.onDestroyView();
    }

    @Override
    public void setupSearchListener(String query) {
        if (!TextUtils.isEmpty(query)) {
            searchItem.expandActionView();
            searchView.setQuery(query, false);
        }
        searchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        onSearchTextChange(query);
                        onSearch();
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(final String newText) {
                        onSearchTextChange(newText);
                        if (newText != null && newText.length() >= MIN_QUERY_LENGTH) {
                            if (inputTimer != null) {
                                inputTimer.cancel();
                            }
                            inputTimer = new Timer();
                            inputTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    Activity activity = getActivity();
                                    if (activity != null) {
                                        getActivity().runOnUiThread(SearchFragment.this::onSearch);
                                    }
                                }
                            }, INPUT_SEARCH_DELAY_MS);
                        }
                        return false;
                    }
                });
    }

    @Override
    protected BasePresenter getBasePresenter() {
        return presenter;
    }
}